Template = {
  setup : function(){
    Template.events.map();
    Data.api.ajax_get_businesses();
  },
  events: {
    map: function(){
      Map.declare_token();  
      Map.make();
      Map.inst_mapi_geocoder();
    },
    listeners: {
      map : function(){
        /*
        Map.moveMap(Map.redraw); //sets map to #select value
        Map.map.on('moveend', function onMapMoveend() {
         Data.listings.getListings();
        });
        Map.map.on('zoomend', function onMapZoomend() {
         Data.listings.getListings();
        });
        */
        //only get listings once
      }
    },
  },
}
Data = {
  mapbounds : {},
  markers: {},
  ownership_self_certs: [],
  business_types: [],
  api : {
    ajax_get_businesses : function(){
      var mb = Data.mapbounds;
      mb = Map.map.getBounds();
      mb.sw = mb._southWest;
      mb.ne = mb._northEast;
      mb.coordinates = {
        swlat: mb.sw.lat,
        swlng: mb.sw.lng,
        nelat: mb.ne.lat,
        nelng: mb.ne.lng,
        nwlat: mb.ne.lat,
        nwlng: mb.sw.lng,
        selat: mb.sw.lat,
        selng: mb.ne.lng,
      }
      if (Data.mapbounds) {
        //console.log('mapbounds',mapbounds);
        return $.ajax({
          url:'/api/get_businesses/', 
          type: 'POST',
          datatype:'json',
          data: mb.coordinates,
          success: function(d) {
            Data.markers = new L.MarkerClusterGroup()
            console.log(d);
            $(d).each(function(i,di){
              if(di['ownership_self_certs']){
                $(di['ownership_self_certs'].split(',')).each(function(j,diosc){
                  if(Data.ownership_self_certs.indexOf(diosc.trim()) == -1){
                    Data.ownership_self_certs.push(diosc.trim());
                  }
                })
              }
              if(di['business_type']){
                var rdibt = di['business_type'];
                $(di['business_type'].split(',')).each(function(j,dibt){
                  if(Data.business_types.indexOf(dibt.trim()) == -1){
                    Data.business_types.push(dibt.trim());
                  }
                });
              }
              
              var geoposition = di['geoposition'];
              var geoposition_array = geoposition.split(',');
              var title = di['name_and_trade_name_of_firm'];
              if(di['ownership_self_certs']){
                var ownership_array = di['ownership_self_certs'].split(',');
              }
              var marker = L.marker(new L.LatLng(geoposition_array[0], geoposition_array[1]), {
                icon: L.mapbox.marker.icon({
                  'marker-symbol': 'city', 
                  'marker-color': '0044FF',
                }),
              });
              var popupContent = "<div class='popupclass'>"+
                                    "<div class='ptitle'> <b> Company Name </b>"+
                                      di['name_and_trade_name_of_firm'] +
                                    "</div>" +
                                    "<div class='pyearest'> <b> Year Established </b>"+
                                      di['year_established']+
                                    "</div>"+
                                    "<div class='pownerhsip'> <b> Ownership </b>  "+
                                      (di['ownership_self_certs'] || "None Specified") +
                                    "</div>"+
                                    "<div class='pbusinesstype'> <b> Business Type </b>"+
                                      (rdibt || "None Specified") +
                                    "</div>"+
                                    "<div class='paddress'><b> Address </b>"+
                                      "Verification Required." +
                                    "</div>"+
                                    "<div class='pphone'><b> Phone Number </b>"+
                                      "Verification Required." +
                                    "</div>"+
                                  "</div>";

              marker.bindPopup(popupContent,{
                  closeButton: false,
                  minWidth: 320
              });
              Data.markers.addLayer(marker);
            });
            Map.map.addLayer(Data.markers);
            $('.fadethese').fadeOut('slow');
          },
        });
      }
    },
  },
  listings: {
    allListings : null,
    _results : [],
    getListings : function() {
      return Data.api.ajax_get_businesses(function(listings) {
        Data.listings.allListings = listings;
        return Map.redraw();
      });
    },
    filterListings : function(listings) {
      Data.listings._results = [];
      var listings = Data.clone(listings);
      mapBounds = Map.map.getBounds();
      onMap = function(x) {
        if (x.geolocation !== 'None'){
          x.latitude = Number(
            x.geolocation.split(',')[0]);
          x.longitude = Number(
            x.geolocation.split(',')[1]);
          return mapBounds.contains(
            [parseFloat(x.latitude), parseFloat(x.longitude)]);
        }
      };
      good = function(x) {
        console.log(x);
        if (onMap(x)) {
          console.log('is on map');
          return true;
        }
      };
      for (_i = 0, _len = listings.length; _i < _len; _i++) {
        x = listings[_i];
        if (good(x)) {
          Data.listings._results.push(x);
        }
      }
      return Data.listings._results;
    },
    addListing : function(listing, $ct, index) {
      console.log(listing,$ct);
      var bedroom_image;
      if(listing.bedroom_image !== ''){
        bedroom_image = "https://hmsv2.s3.amazonaws.com/"+listing.bedroom_image;
      }else{
        bedroom_image = "/static/img/defaultbedroom.png";
      }
      var $l, hoverIn, hoverOut, ci, co;
      $l = $('.tpl.item').clone().removeClass('tpl hide');
      ci = encodeURIComponent($('#ci').val());
      co = encodeURIComponent($('#co').val());
      $l.attr('href', "/property/" + listing.pk + 
        "/" + listing.slug + "/?ci="+ci+"&co="+co);
      $l.find('.picture').css(
        'background-image', "url("+bedroom_image+")");
      $l.find('.info .price').html(
        Data.listings.ptype_logo_templates[listing.housing_type] +
        //Data.listings.rtype_logo_templates[listing.room_type] +
        " $" + listing.price_per_week + "/  week");
      $l.find('.info .desc').text(
        listing.headline.length <= 15 ? 
        listing.headline : "" + listing.headline.slice(
          0, 25) + "...");
      $l.find('.info').attr('data-marker',listing.pk)
        .attr('data-index','');
      if(!listing.creator_has_soc_img){
        if(listing.creator_image){
          console.log(listing);
            $l.find('img.person').attr(
              'src', 'https://hmsv2.s3.amazonaws.com/'+
              listing.creator_image);
        }else{
            $l.find('img.person').attr(
              'src', '/static/img/default.png');
        }
      }else{
        $l.find('img.person').attr(
          'src', listing.creator_soc_img);
      }
      /*
      hoverIn = function() {
        var marker;
        marker = Map.markers[index];
        if (marker) {
          //marker.properties.icon.html = marker.properties.pulse_template;
          //Map.triggerPopup(marker.properties.pk);
          //return Map.marker_layer.setGeoJSON(Map.geojson);
        }
      };
      hoverOut = function() {
        var marker;
        marker = Map.markers[index];
        if (marker) {
          //marker.properties.icon.html = marker.properties.no_pulse_template;
          //return Map.marker_layer.setGeoJSON(Map.geojson);
        }
      };
      $l.hover(hoverIn, hoverOut);
  */
      $l.hover(function(e){
              var $this = $(this);
              console.log(e,$this,'e and this');
              console.log($this.children('.info').attr('data-marker'));
              var pk = $this.children('.info').attr('data-marker');
              Map.triggerPopup(Number(pk));
            }); 
      return $l.appendTo($ct);
    },  
    showListings : function(listings) {
      console.log('sdf');
      console.log(listings);
      var $ct, $l, marker, x, _i, _len;
      $ct = $('#ct').empty();
      Map.clearMarkers();
      for (_i = 0, _len = listings.length; _i < _len; _i++) {
        x = listings[_i];
        console.log(x);
        $l = Data.listings.addListing(x, $ct,_i);
        xlatLng = x.geolocation.split(',');
        marker = Map.addMarker({
          lat: Number(xlatLng[0]),
          lng: Number(xlatLng[1]),
        }, x, $l);
        console.log(marker);
      }
      $('#result-cnt').text(
        "" + listings.length + " Result" + (
          listings.length === 1 ? '' : 's'));
      return Map.setMarkers();
    },
  },
  pad : function(x) {
    if (x < 10) {
      return "0" + x;
    } else {
      return x;
    }
  },
  clone : function(x) {
    return JSON.parse(JSON.stringify(x || null));
  },
}
Map = {
  map : null,
  default_loc : 'Southern Methodist University',
  mapi_geocoder : null,
  markers: [],
  mapi : google.maps, 
  marker_layer : null,
  marker_elements : {},
  county: $('#county'),
  declare_token : function(){
    L.mapbox.accessToken = 'pk.eyJ1IjoiaW1ob3RlcCIsImEiOiJTa0ltby04In0.pL0Ir879LXCaqAsXbXn2JQ';
  },
  make : function(){
    Map.map = L.mapbox.map('map', 'imhotep.kf3ncedg').setView(
      [32.7766642,-96.79698789999998],5);
    //Map.setMarkerLayer();
    //Map.set_counties();
  },
  geojson: [],
  inst_mapi_geocoder : function(){
    Map.mapi_geocoder = new Map.mapi.Geocoder();
  },
  getCurrentLocation : function(cb) {
    if(Map.mapi_geocoder == null){
      Map.inst_mapi_geocoder();
    }
    var area;
    area = $('#select').val();
    return Map.mapi_geocoder.geocode({
        address: area
    }, function(results, status) {
        if (status === Map.mapi.GeocoderStatus.OK) {
            console.log(results[0]);
            return cb(results[0].geometry.location);
        }
    });
  },
  clearMarkers : function() {
    var marker, _;
    for (_ in Map.markers) {
      marker = Map.markers[_];
      //marker.setMap(null);
    }
    return Map.markers = [];
    Map.geojson = [];
  },
  geojsonMarkerOptions : {
    radius: 8,
    fillColor: "#ff7800",
    color: "#000",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8,
    content:'sdfdfs'
  },
  addMarker : function(pos, listing, $div) {
    var bedroom_image;
    if(listing.bedroom_image){
      bedroom_image = "https://hmsv2.s3.amazonaws.com/"+listing.bedroom_image;
    }else{
      bedroom_image = "/static/img/defaultbedroom.png";
    }
    var marker_object = {
      type: 'Feature',
      geometry: { 
        type: "Point", 
        coordinates: [parseFloat(pos.lng), parseFloat(pos.lat)]
      },
      properties: {
        image: bedroom_image,
        url: "/property/"+listing.pk+"/"+listing.slug,
        'marker-color': "#208e62",
        'marker-size': "large",
        headline: listing.headline,
        ptype: listing.housing_type,
        rtype: listing.room_type,
        ppw: listing.price_per_week,
        street: listing.street,
        pulse_template: "<div class='marker'><span class='tr45'>$"+listing.price_per_week+"</span></div><div class='pulse'></div>",
        no_pulse_template: "<div class='marker'><span class='tr45'>$"+listing.price_per_week+"</span></div>",
        pk:listing.pk,
        "icon": {
            "className": "", // class name to style
            'marker-color':'blue',
            "html": "<div class='marker'><span class='tr45'>$"+listing.price_per_week+"</span></div>", // add content inside the marker
            "iconSize": null // size of icon, use null to set the size in CSS
          }
      },
    };
    
    console.log(marker_object);
    //Map.setMarkerStyles();
    Map.markers.push(marker_object);
    return Map.geojson.push(marker_object);

    /*
    Map.markers[listing.id] = marker;
    return marker;
    */
    
  },
  setMarkerStyles : function(){
      L.geoJson(Map.geojson, {
        pointToLayer : function (feature, latlng) {
          return L.circleMarker(latlng, Map.geojsonMarkerOptions);
        },
      }).addTo(Map.map);
  },
  setMarkerLayer : function(){
    resetMarkerPulses = function(){
      $(Map.markers).each(function(i,marker){
        marker.properties.icon.html = marker.properties.no_pulse_template;  
      })
      Map.marker_layer.setGeoJSON(Map.geojson);
    }
    Map.marker_layer = L.mapbox.featureLayer().addTo(Map.map);
    /*
    Map.marker_layer.on('mouseover', function(e) {
      console.log(e);
      e.layer.feature.properties.icon.html = e.layer.feature.properties.pulse_template;
      Map.marker_layer.setGeoJSON(Map.geojson);
    }); 

    Map.map.on('mouseout', resetMarkerPulses);
    */
  },
  setMarkers: function(){
    Map.marker_layer.on('layeradd', function(e) {
        var marker = e.layer,
        feature = marker.feature;
        // Create custom popup content
        var popupContent =  '<div class="goobeh">'+
                              '<a target="_blank" class="popup" href="' + feature.properties.url + '">' +
                                '<div class="popupheadline">'+feature.properties.headline+'</div>'+
                                '<img class="popupimage" src="' + feature.properties.image + '" />'+
                              '</a>' +
                              '<div class="popupdesc">'+
                                '<div class="popupdescitem">'+
                                  '<div class="puditop">'+
                                    Data.listings.ptype_logo_templates[feature.properties.ptype] +
                                  '</div>'+
                                  '<div class="pudibottom">'+
                                    feature.properties.ptype+
                                  '</div>'+
                                '</div>'+
                                '<div class="popupdescitem">'+
                                  '<div class="puditop">'+
                                    Data.listings.rtype_logo_templates[feature.properties.rtype] +
                                  '</div>'+
                                  '<div class="pudibottom">'+
                                    feature.properties.rtype+
                                  '</div>'+
                                '</div>'+
                                '<div class="popupdescitem">'+
                                  '<div class="puditop">$'+
                                    feature.properties.ppw +
                                  '</div>'+
                                  '<div class="pudibottom">'+
                                    'Per Week'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                            '<div>';

        marker.bindPopup(popupContent,{
            closeButton: false,
            minWidth: 320
        });

        //add icon
        marker.setIcon(L.divIcon(feature.properties.icon));
    });

    Map.marker_layer.setGeoJSON(Map.geojson);
  },
  redraw : function() {
    if (Data.listings.allListings) {
      return Data.listings.showListings(
        Data.listings.filterListings(Data.listings.allListings));
    }
  },
  moveMap : function(cb) {
    console.log('movemap');
    return Map.getCurrentLocation(function(loc) {
      console.log(loc);
      Map.map.setView([loc.lat(),loc.lng()],13);
      return cb();
    });
  },
  triggerPopup : function(pk){
    Map.marker_layer.eachLayer(function(marker) {
        if (marker.feature.properties.pk === pk) {
          if(Object.keys(Map.marker_elements).indexOf(pk) !== 0){
            Map.marker_elements[pk] = marker;
          }
          marker.openPopup();
        }
    });
  },
  set_counties : function(){
    /*
    $.ajax({
        url: '/static/assets/us-counties.geojson',
        dataType: 'json',
        success: function load(d) {
            var counties = L.geoJson(d).addTo(Map.map);
            L.marker([38, -102], {
                icon: L.mapbox.marker.icon({
                    'marker-color': '#f86767'
                }),
                draggable: true
            }).addTo(Map.map)
            .on('dragend', function(e) {
                var layer = leafletPip.pointInLayer(this.getLatLng(), counties, true);
                console.log(layer);
                if (layer.length) {
                  Map.county.innerHTML = '<strong>' + layer[0].feature.properties.name + '</strong>';
                } else {
                  Map.county.innerHTML = '';
                }
            });
        },
        error: function(request,state,error){
          console.log(error);
        }
    });
*/
  }
}
$(document).ready(function(){
  Template.setup();
});