import os
import pandas as pd
import numpy as np

#for fn in os.listdir('../africanawiki_geodata/static/xls'):
wd = '../africanawiki_geodata/static/xls/'
frames = []
all_data = pd.DataFrame()
for fn in os.listdir(wd):
	df = pd.read_html(wd+fn)
	print 'appending %s to dataframe...' % (fn)
	all_data = all_data.append(df,ignore_index=True)
	

print all_data
all_data.to_csv('master.csv',encoding='utf-8')