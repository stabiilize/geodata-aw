#AfricanaWiki Data Technical Documentation 

Site: http://data.africanawiki.org

Stack: 

 - Backend: Python (2.7) & Django (1.8)
 - Database: PostgreSQL (9.3)


### How do I get set up? ###

A. Configuration

   0. Install Git
   1. Clone this repository in a local projects folder
   2. Install the dependencies (part B) - install pip if you haven't already
   3. Create a local postgresql database and sync the database (part C)
   4. Create a branch for you to work in (git checkout -b mybranch)
   5. Push changes (git push origin mybranch)

B. Dependencies:
      
        Django==1.8.6
        dj-database-url==0.3.0
        dj-static==0.0.6
        gunicorn==19.1.1
        psycopg2==2.5.1
        static==0.4
        wsgiref==0.1.2
        django-postgres-copy==0.0.6
        django-geoposition==0.2.2
        geopy==1.11.0
        python-omgeo==1.7.2
        geocoder==1.6.4
        djangorestframework==3.3.0
        PyYAML==3.11
        django-pipeline==1.5.4
        django-allauth==0.24.1
        django-invitations==1.2
        pillow==2.3.1
        django-simple-captcha==0.4.6
        djrill==1.3.0
	
	(all of these can be found in requirements.txt; you can do "pip install -r requirements.txt" in the project folder you cloned this repo to to install them all)

C. Database configuration

      - Install PostgreSQL 
      - Create database called "africanawiki_geodata" (you can use pgAdmin for this)
      - Create a local user called "postgres" and password called "USAfricana720424" (or whatever you want)
      - If you choose a different user/password combination, go into "/africanwiki_geodata/settings.py" and edit the DATABASES = {'default': dj_database_url.config(default='postgres://postgres:USAfricana720424@localhost:5432/africanawiki_geodata')} to match your settings

      - Sync the database for the first time with "python manage.py syncdb", create your superuser (admin) account
      - All database syncs after the first sync will use "manage.py migrate"


### Tests ###

* Test to see if your postgres db is working by doing 'python manage.py shell' in console and doing:

        from django.contrib.auth.models import User
        User.objects.all()
        #Your db is running if your superuser is displayed
		
### Who do I talk to? ###

* self@mikejohnsonjr.com