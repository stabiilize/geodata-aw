from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from .views import get_businesses
from engine.models import SBABusiness
from .serializers import BusinessSerializer
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticatedOrReadOnly,IsAuthenticated

@permission_classes([IsAuthenticatedOrReadOnly])
class BusinessViewSet(viewsets.ModelViewSet):
    queryset = SBABusiness.objects.all()
    serializer_class = BusinessSerializer

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'businesses',BusinessViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^get_businesses', get_businesses,name="get_businesses")
]