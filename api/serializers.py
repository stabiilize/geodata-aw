from django.contrib.auth.models import User
from engine.models import SBABusiness
from rest_framework import serializers

class BusinessSerializer(serializers.ModelSerializer):
    class Meta:
        model = SBABusiness
        fields = (
        	'name_and_trade_name_of_firm', 
        	'geoposition', 
        	'ownership_self_certs', 
        	'business_type', 
        	'year_established',
        	'capabilities_narrative',
        	'keywords',
        	'address',
        	'phone_number',
        	'www_page_url',
        	'contact',
        	'legal_structure',
        )