from django.contrib.auth.models import User
from engine.models import SBABusiness
from .serializers import BusinessSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticatedOrReadOnly,IsAuthenticated
from rest_framework.decorators import api_view,permission_classes

@api_view(['GET','POST'])
@permission_classes([IsAuthenticatedOrReadOnly])
def get_businesses(request):
	if request.method == 'POST':
		"""
		print request.POST

		poly = request.POST
		bbPath = mplPath.Path(
			np.array([
				[poly[u'swlat'],poly[u'swlng']],
				[poly[u'nwlat'],poly[u'nwlng']],
				[poly[u'nelat'],poly[u'nelng']],
				[poly[u'selat'],poly[u'selng']],
			]))

		businesses = bbPath.contains_point((200, 100))
		"""
		businesses = SBABusiness.objects.all()
		serializer = BusinessSerializer(businesses, many=True)
		return Response(serializer.data)

