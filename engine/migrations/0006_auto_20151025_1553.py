# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0005_auto_20151025_1553'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbabusiness',
            name='capabilities_narrative',
            field=models.TextField(null=True, blank=True),
        ),
    ]
