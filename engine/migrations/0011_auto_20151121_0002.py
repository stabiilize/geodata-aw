# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0010_invitationrequest_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='invitationrequest',
            name='github_url',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='invitationrequest',
            name='is_declined',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='invitationrequest',
            name='linkedin_url',
            field=models.URLField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='invitationrequest',
            name='reddit_url',
            field=models.URLField(null=True, blank=True),
        ),
    ]
