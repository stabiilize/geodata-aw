# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0003_sbabusiness_empty_field'),
    ]

    operations = [
        migrations.AddField(
            model_name='sbabusiness',
            name='hashtag_field',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
