# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0014_auto_20151121_0056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invitationrequest',
            name='timestamped_photo',
            field=models.ImageField(null=True, upload_to=b'uploaded_images', blank=True),
        ),
    ]
