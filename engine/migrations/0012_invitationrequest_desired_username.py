# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0011_auto_20151121_0002'),
    ]

    operations = [
        migrations.AddField(
            model_name='invitationrequest',
            name='desired_username',
            field=models.CharField(default='default', unique=True, max_length=20),
            preserve_default=False,
        ),
    ]
