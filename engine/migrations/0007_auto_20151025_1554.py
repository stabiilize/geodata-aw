# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0006_auto_20151025_1553'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbabusiness',
            name='congressional_district',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
