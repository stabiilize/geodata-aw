# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0004_sbabusiness_hashtag_field'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbabusiness',
            name='cage_code',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
