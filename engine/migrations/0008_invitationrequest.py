# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0007_auto_20151025_1554'),
    ]

    operations = [
        migrations.CreateModel(
            name='InvitationRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254)),
                ('full_name', models.CharField(max_length=255)),
                ('facebook_url', models.URLField(null=True, blank=True)),
                ('twitter_url', models.URLField(null=True, blank=True)),
                ('timestamped_photo', models.ImageField(null=True, upload_to=b'/uploaded_images', blank=True)),
                ('bio', models.TextField(null=True, blank=True)),
                ('is_user_accepted', models.BooleanField(default=False)),
                ('is_admin_accepted', models.BooleanField(default=False)),
            ],
        ),
    ]
