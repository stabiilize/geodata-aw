# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sbabusiness',
            name='_8a_case',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='_8a_entrance_date',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='_8a_exit_date',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='business_type',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='cage_code',
            field=models.CharField(max_length=5, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='capabilities_narrative',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='congressional_district',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='contact',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='county_code',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='dbe_states',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='duns_number',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='email_address',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='email_page_url',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='export_business_activities',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='fax_number',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='fiscal_year',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='hubzone_certification_date',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='hubzone_exit_date',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='in_sam',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='in_tech_net',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='incorporation_country',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='incorporation_state',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='is_exporter',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='keywords',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='last_updated',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='legal_structure',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='main_or_branch',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='metro_stat_area',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='name_and_trade_name_of_firm',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='ownership_self_certs',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='phone_number',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='sdb_entrance_date',
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='sdb_exit_date',
            field=models.CharField(max_length=256, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='www_page_url',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sbabusiness',
            name='year_established',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='sbabusiness',
            name='address',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
