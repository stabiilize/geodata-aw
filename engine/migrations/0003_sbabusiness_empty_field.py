# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0002_auto_20151025_1223'),
    ]

    operations = [
        migrations.AddField(
            model_name='sbabusiness',
            name='empty_field',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
