# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0013_dataset_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invitationrequest',
            name='email',
            field=models.EmailField(unique=True, max_length=254),
        ),
    ]
