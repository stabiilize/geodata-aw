# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0012_invitationrequest_desired_username'),
    ]

    operations = [
        migrations.AddField(
            model_name='dataset',
            name='image',
            field=models.ImageField(null=True, upload_to=b'uploaded_images', blank=True),
        ),
    ]
