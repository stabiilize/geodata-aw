# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0009_dataset'),
    ]

    operations = [
        migrations.AddField(
            model_name='invitationrequest',
            name='slug',
            field=models.SlugField(null=True, blank=True),
        ),
    ]
