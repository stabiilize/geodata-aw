from django.shortcuts import render
from django.conf import settings
from .models import InvitationRequest
from .models import Dataset
from .forms import InvitationRequestForm
from .forms import DatasetForm
from .forms import CustomInviteForm
from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.contrib.messages.views import SuccessMessageMixin
from django.http import Http404, HttpResponseRedirect
from invitations.models import Invitation
from django.views.generic import FormView, View
from django.views.generic.detail import SingleObjectMixin
from invitations.app_settings import app_settings
from invitations.forms import InviteForm
from allauth.account.adapter import get_adapter
from invitations import signals
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.models import User

def home(request):
	context = {}
	print settings.DEBUG
	return render(request,
				  'home.html',
				  context)

def public_datasets(request):
	context = {}
	return render(request,
				  'public_datasets.html',
				  context)

def dashboard(request):
	context = {}
	requests = [u for u in InvitationRequest.objects.all() if not User.objects.filter(email=u.email)]

	context['requests'] = requests
	return render(request,
				  'engine/dashboard.html',
				  context)

def geo_minority_businesses(request):
	context = {}
	return render(request,
				  'geohome.html',
				  context)

def request_an_invite(request):
	context = {}
	return render(request,
				  'request_an_invite.html',
				  context)

class InvitationRequestCreateView(SuccessMessageMixin,CreateView):
	model = InvitationRequest
	form_class = InvitationRequestForm
	template_name = 'engine/invitation_request_create.html'
	success_message = "You've successfully requested an invite. We'll get back to you as soon as possible :)"

	def form_valid(self,form):
		self.object = form.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self,form):
		return self.render_to_response(
			self.get_context_data(form=form,))

class InvitationRequestDetailView(DetailView):
	model = InvitationRequest
	template_name = 'engine/invitation_request_detail.html'

	def get_success_url(self):
		return reverse("invitation_request_detail", kwargs={
			'pk':str(self.kwargs['pk']),
			'slug':str(self.kwargs['slug'])})

	def get_context_data(self,**kwargs):
		context = super(InvitationRequestDetailView, self).get_context_data(**kwargs)
		return context

class DatasetCreateView(CreateView):
	model = Dataset
	form_class = DatasetForm
	template_name = 'engine/dataset_create.html'

	def form_valid(self,form):
		form = DatasetForm(self.request.POST,self.request.FILES)
		f = form.save(commit=False)
		f.save()
		return super(DatasetCreateView,self).form_valid(form)

class InviteUserView(CreateView):
	model = Invitation
	form_class = CustomInviteForm
	
	template_name = 'engine/invite_user.html'
	def form_valid(self,form):
		email = form.cleaned_data["email"]

		try:
			invite = form.save(email)
			invite.send_invitation(self.request)
		except Exception:
			return self.form_invalid(form)

		return self.render_to_response(
			self.get_context_data(
				success_message='%s has been invited' % email))

	def form_invalid(self, form):
		return self.render_to_response(self.get_context_data(form=form))


class AcceptInviteView(SingleObjectMixin, View):
	form_class = InviteForm
	template_name = 'engine/accept_invite.html'

	def get(self, *args, **kwargs):
		if settings.CONFIRM_INVITE_ON_GET:
			return self.post(*args, **kwargs)
		else:
			raise Http404()

	def post(self, *args, **kwargs):
		self.object = invitation = self.get_object()

		invitation.accepted = True
		invitation.save()
		get_adapter().stash_verified_email(self.request, invitation.email)

		signals.invite_accepted.send(sender=self.__class__,
									 request=self.request,
									 email=invitation.email)

		get_adapter().add_message(self.request,
								  messages.SUCCESS,
								  'invitations/messages/invite_accepted.txt',
								  {'email': invitation.email})

		return redirect(app_settings.SIGNUP_REDIRECT)

	def get_object(self, queryset=None):
		if queryset is None:
			queryset = self.get_queryset()
		print queryset
		try:
			print self.kwargs

			return Invitation.objects.get(key=self.kwargs["key"].lower())
		except Invitation.DoesNotExist:
			raise Http404()

	def get_queryset(self):
		return Invitation.objects.all_valid()