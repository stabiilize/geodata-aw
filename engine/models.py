from django.conf import settings
from django.db import models
from geoposition.fields import GeopositionField
import geocoder
from omgeo import Geocoder
import geopy
from django.db.models import Lookup
from django.db.models.fields import Field
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse

class SBABusiness(models.Model):
	name_and_trade_name_of_firm = models.CharField(max_length=255,blank=True,null=True)
	contact = models.CharField(max_length=255,blank=True,null=True)
	address = models.CharField(max_length=255,blank=True,null=True)
	capabilities_narrative = models.TextField(blank=True,null=True)
	geoposition = GeopositionField(blank=True,null=True)
	cage_code = models.CharField(max_length=255,blank=True,null=True)
	congressional_district = models.CharField(max_length=255,blank=True,null=True)
	county_code = models.IntegerField(blank=True,null=True)
	duns_number = models.CharField(max_length=20,blank=True,null=True)
	email_address = models.EmailField(blank=True,null=True)
	email_page_url = models.CharField(max_length=255,blank=True,null=True)
	fax_number = models.CharField(max_length=20,blank=True,null=True)
	fiscal_year = models.CharField(max_length=20,blank=True,null=True)
	in_sam = models.CharField(max_length=255,blank=True,null=True) #change to boolean field at some point
	in_tech_net = models.CharField(max_length=255,blank=True,null=True) #change to boolean field at some point
	incorporation_country = models.CharField(max_length=255,blank=True,null=True)
	incorporation_state = models.CharField(max_length=255,blank=True,null=True)
	last_updated = models.CharField(max_length=255,blank=True,null=True)
	main_or_branch = models.CharField(max_length=255,blank=True,null=True)
	metro_stat_area = models.CharField(max_length=255,blank=True,null=True)
	phone_number = models.CharField(max_length=255,blank=True,null=True)
	www_page_url = models.CharField(max_length=255,blank=True,null=True)
	year_established = models.CharField(max_length=255,blank=True,null=True)
	_8a_case = models.CharField(max_length=255,blank=True,null=True)
	_8a_entrance_date = models.CharField(max_length=255,blank=True,null=True)
	_8a_exit_date = models.CharField(max_length=255,blank=True,null=True)
	hubzone_certification_date = models.CharField(max_length=255,blank=True,null=True)
	hubzone_exit_date = models.CharField(max_length=255,blank=True,null=True)
	legal_structure = models.CharField(max_length=255,blank=True,null=True)
	sdb_entrance_date = models.CharField(max_length=256,blank=True,null=True)
	sdb_exit_date = models.CharField(max_length=256,blank=True,null=True)
	keywords = models.TextField(blank=True,null=True)
	is_exporter = models.CharField(max_length=20,blank=True,null=True) #change to boolean field at some point
	business_type = models.TextField(blank=True,null=True)
	dbe_states = models.TextField(blank=True,null=True)
	export_business_activities = models.TextField(blank=True,null=True)
	ownership_self_certs = models.TextField(blank=True,null=True)

	empty_field = models.CharField(max_length=50,blank=True,null=True)
	hashtag_field = models.CharField(max_length=50,blank=True,null=True)

	def __unicode__(self):
		return unicode(self.name_and_trade_name_of_firm)

	def save(self, *args, **kwargs):
		if not self.geoposition:
			
			#omgeo
			"""
			g = Geocoder()
			result = g.geocode(self.address)
			try:
				self.geoposition = [
					result['candidates'][0].y, #switched for ERSI
					result['candidates'][0].x,
				]
			except IndexError:
				print 'error'
				print result
			"""
			#google
			"""
			g = geocoder.google(self.address)
			self.geoposition = g.latlng
			
			"""
			#bing via geopy
			#g = geopy.geocoders.Bing(
			#	api_key='Ao1fBnmH6itsPp9k4DOf5_3X5DCdVUEVp8SOnVpSwpUAse2sMNzOioQq0fCtlfxb')
			
			#DataBC via geopy
			g = geopy.geocoders.DataBC()
			
			#Yandex via geopy
			#g = geopy.geocoders.Yandex()
			
			result = g.geocode(self.address)
			try:
				self.geoposition = [
					result.latitude,
					result.longitude
				]
			except AttributeError:
				'error'
				print result
			print self.address,self.geoposition

		super(SBABusiness,self).save(*args,**kwargs)

class InvitationRequest(models.Model):
	email = models.EmailField(unique=True)
	desired_username = models.CharField(max_length=20,unique=True)
	full_name = models.CharField(max_length=255)
	facebook_url = models.URLField(blank=True,null=True)
	twitter_url = models.URLField(blank=True,null=True)
	linkedin_url = models.URLField(blank=True,null=True)
	github_url = models.URLField(blank=True,null=True)
	reddit_url = models.URLField(blank=True,null=True)
	timestamped_photo = models.ImageField(upload_to="uploaded_images",blank=True,null=True)
	bio = models.TextField(blank=True,null=True)
	is_user_accepted = models.BooleanField(default=False)
	is_admin_accepted = models.BooleanField(default=False)
	is_declined = models.BooleanField(default=False)
	slug = models.SlugField(blank=True,null=True)

	def __unicode__(self):
		return unicode(self.full_name)

	def save(self, *args, **kwargs):
		if not self.slug:
			#Newly created object, so set slug
			self.slug = slugify(self.full_name)
		super(InvitationRequest,self).save(*args,**kwargs)

	def get_absolute_url(self):
		return reverse("invitation_request_detail", kwargs={'pk':str(self.id),'slug':str(self.slug)})

class Dataset(models.Model):
	name = models.CharField(max_length=255)
	link = models.EmailField()
	description = models.TextField(blank=True,null=True)
	image = models.ImageField(upload_to='uploaded_images',null=True,blank=True)

	def __unicode__(self):
		return unicode(self.name)
"""
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
	if created:
		Token.objects.create(user=instance)
"""