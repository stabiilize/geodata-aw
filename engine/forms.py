from django import forms
from .models import InvitationRequest
from .models import Dataset
from captcha.fields import CaptchaField
from django.utils.translation import ugettext_lazy as _
from allauth.account.adapter import get_adapter
from invitations.models import Invitation
from invitations.forms import CleanEmailMixin

class InvitationRequestForm(forms.ModelForm):
	captcha = CaptchaField()
	class Meta:
		model = InvitationRequest
		exclude = ('is_admin_accepted','is_user_accepted','slug','is_declined')

class DatasetForm(forms.ModelForm):
	class Meta:
		model = Dataset
		exclude = ()

class CustomInviteForm(forms.ModelForm, CleanEmailMixin):
	captcha = CaptchaField()
	email = forms.EmailField(
		label=_("E-mail"),
		required=True,
		widget=forms.TextInput(attrs={"type": "email", "size": "30"}))
	class Meta:
		model = Invitation
		exclude = ('accepted','created','sent','key')

	def save(self, email):
		return Invitation.create(email=email)