Template = {
  setup : function(){
    Template.events.map();
    Data.api.ajax_get_businesses();
  },
  events: {
    map: function(){
      Map.declare_token();  
      Map.make();
      Map.inst_mapi_geocoder();
    },
    listeners: {
      map : function(){
        //pass
      }
    },
  },
}
Data = {
  mapbounds : {},
  markers: {},
  ownership_self_certs: [],
  business_types: [],
  business_names: [],
  filtering : {
    search_businesses : function(val){

    },
    filter_businesses_by_type : function(type){

    },
    filter_by_self_cert : function(self_cert){

    },
  },
  api : {
    ajax_get_businesses : function(){
      var mb = Data.mapbounds;
      mb = Map.map.getBounds();
      mb.sw = mb._southWest;
      mb.ne = mb._northEast;
      mb.coordinates = {
        swlat: mb.sw.lat,
        swlng: mb.sw.lng,
        nelat: mb.ne.lat,
        nelng: mb.ne.lng,
        nwlat: mb.ne.lat,
        nwlng: mb.sw.lng,
        selat: mb.sw.lat,
        selng: mb.ne.lng,
      }
      if (Data.mapbounds) {
        //console.log('mapbounds',mapbounds);
        return $.ajax({
          url:'/api/get_businesses/', 
          type: 'POST',
          datatype:'json',
          data: mb.coordinates,
          success: function(d) {
            Data.markers = new L.MarkerClusterGroup()
            console.log(d);
            $(d).each(function(i,di){

              if(di['ownership_self_certs']){
                $(di['ownership_self_certs'].split(',')).each(function(j,diosc){
                  if(Data.ownership_self_certs.indexOf(diosc.trim()) == -1){
                    Data.ownership_self_certs.push(diosc.trim());
                  }
                })
              }
              if(di['business_type']){
                var rdibt = di['business_type'];
                $(di['business_type'].split(',')).each(function(j,dibt){
                  if(Data.business_types.indexOf(dibt.trim()) == -1){
                    Data.business_types.push(dibt.trim());
                  }
                });
              }
              
              var geoposition = di['geoposition'];
              var geoposition_array = geoposition.split(',');
              var title = di['name_and_trade_name_of_firm'];
              if(di['ownership_self_certs']){
                var ownership_array = di['ownership_self_certs'].split(',');
              }
              var marker = L.marker(new L.LatLng(geoposition_array[0], geoposition_array[1]), {
                icon: L.mapbox.marker.icon({
                  'marker-symbol': 'city', 
                  'marker-color': '0044FF',
                }),
              });
              var popupContent = "<div class='popupclass'>"+
                                    "<div class='ptitle'> <b> Company Name </b>"+
                                      di['name_and_trade_name_of_firm'] +
                                    "</div>" +
                                    "<div class='pyearest'> <b> Year Established </b>"+
                                      di['year_established']+
                                    "</div>"+
                                    "<div class='pownerhsip'> <b> Ownership </b>  "+
                                      (di['ownership_self_certs'] || "None Specified") +
                                    "</div>"+
                                    "<div class='pbusinesstype'> <b> Business Type </b>"+
                                      (rdibt || "None Specified") +
                                    "</div>"+
                                    "<div class='pcapnar'><b> Capabilities Narrative </b>"+
                                      (di['capabilities_narrative'] || "None Specified") +
                                    "</div>"+
                                    "<div class='pkeywords'><b> Keywords </b>"+
                                      (di['keywords'] || "None Specified") +
                                    "</div>"+
                                    "<div class='address'><b> Address </b>"+
                                      (di['address'] || "None Specified") +
                                    "</div>"+
                                    "<div class='pphone'><b> Phone Number </b>"+
                                      (di['phone_number'] || "None Specified") +
                                    "</div>"+
                                    "<div class='pwwwpageurl'><b> Website </b>"+
                                      (di['www_page_url'] || "None Specified") +
                                    "</div>"+
                                    "<div class='pcontact'><b> Contact </b>"+
                                      (di['contact'] || "None Specified") +
                                    "</div>"+
                                    "<div class='plegal'><b> Legal Structure </b>"+
                                      (di['legal_structure'] || "None Specified") +
                                    "</div>"+
                                  "</div>";

              marker.bindPopup(popupContent,{
                  closeButton: false,
                  minWidth: 320
              });
              Data.business_names.push(title);
              Data.markers.addLayer(marker);
            });
            Map.map.addLayer(Data.markers);
            Data.fill_inputs();
            $('.fadethese').fadeOut('slow');
          },
        });
      }
    },
  },
  fill_inputs : function(){
    $(Data.business_types).each(function(i,btype){
              var opt = $('<option/>',{
                value: btype,
                text:btype,
              });
              $(".business_type").append(opt);
            });
            $(Data.ownership_self_certs).each(function(i,ownership){
              var opt = $('<option/>',{
                value: ownership,
                text:ownership,
              });
              $(".ownership").append(opt);
            });
            $(".business_name_search").autocomplete({
              source: Data.business_names
            });
  } 
}
Map = {
  map : null,
  mapi_geocoder : null,
  markers: [],
  mapi : google.maps, 
  marker_layer : null,
  marker_elements : {},
  county: $('#county'),
  declare_token : function(){
    L.mapbox.accessToken = 'pk.eyJ1IjoiaW1ob3RlcCIsImEiOiJTa0ltby04In0.pL0Ir879LXCaqAsXbXn2JQ';
  },
  make : function(){
    Map.map = L.mapbox.map('map', 'imhotep.kf3ncedg').setView(
      [32.7766642,-96.79698789999998],5);
    //Map.setMarkerLayer();
    //Map.set_counties();
  },
  geojson: [],
  inst_mapi_geocoder : function(){
    Map.mapi_geocoder = new Map.mapi.Geocoder();
  },
  set_counties : function(){
    /*
    $.ajax({
        url: '/static/assets/us-counties.geojson',
        dataType: 'json',
        success: function load(d) {
            var counties = L.geoJson(d).addTo(Map.map);
            L.marker([38, -102], {
                icon: L.mapbox.marker.icon({
                    'marker-color': '#f86767'
                }),
                draggable: true
            }).addTo(Map.map)
            .on('dragend', function(e) {
                var layer = leafletPip.pointInLayer(this.getLatLng(), counties, true);
                console.log(layer);
                if (layer.length) {
                  Map.county.innerHTML = '<strong>' + layer[0].feature.properties.name + '</strong>';
                } else {
                  Map.county.innerHTML = '';
                }
            });
        },
        error: function(request,state,error){
          console.log(error);
        }
    });
*/
  }
}
$(document).ready(function(){
  Template.setup();
});