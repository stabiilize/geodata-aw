from engine.models import SBABusiness
from postgres_copy import CopyMapping
from django.core.management.base import BaseCommand
from django.conf import settings

class Command(BaseCommand):
	def handle(self,*args,**kwargs):
		c = CopyMapping(
			SBABusiness,
			settings.BASE_DIR+'/africanawiki_geodata/static/csv/sbabusiness/master.csv',
			dict(
				empty_field='index',
				hashtag_field='hashtag',
				name_and_trade_name_of_firm='Name and Trade Name of Firm',
				contact='Contact',
				address='Address and City, State Zip',
				capabilities_narrative='Capabilities Narrative',
				cage_code='CAGE Code',
				congressional_district='Congr. Dist.',
				county_code='County Code',
				duns_number='DUNS Number',
				email_address='E-mail Address',
				email_page_url='E-Mall Page URL',
				fax_number='Fax Number',
				fiscal_year='Fiscal Year Ends',
				in_sam='In SAM?',
				in_tech_net='In TECH-Net?',
				incorporation_country='Incorp. Country',
				incorporation_state='Incorp. State',
				last_updated='Last Updated',
				main_or_branch='Main/Branch',
				metro_stat_area='Metro Stat Area',
				phone_number='Phone Number',
				www_page_url='WWW Page URL',
				year_established='Year Established',
				_8a_case='8(a) Case Number',
				_8a_entrance_date='8(a) Entrance Date',
				_8a_exit_date='8(a) Exit Date',
				hubzone_certification_date='HUBZone Certification Date',
				hubzone_exit_date='HUBZone Exit Date',
				legal_structure='Legal Structure',
				sdb_entrance_date='SDB Entrance Date',
				sdb_exit_date='SDB Exit Date',
				keywords='Keywords',
				is_exporter='Exporter?',
				business_type='Business Type (Service, Construction, etc)',
				dbe_states='DBE States',
				export_business_activities='Export Business Activities',
				ownership_self_certs='Ownership/Self-Certs (Minority, Veteran, Woman, etc)'
			)
		)
		c.save()