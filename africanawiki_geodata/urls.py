from django.conf.urls import patterns, include, url
from django.contrib import admin
import api
from rest_framework.authtoken import views
from engine.views import home
from engine.views import public_datasets
from engine.views import dashboard
from engine.views import request_an_invite
from engine.views import geo_minority_businesses
from engine.views import InvitationRequestCreateView
from engine.views import InvitationRequestDetailView
from engine.views import DatasetCreateView
from engine.views import InviteUserView
from engine.views import AcceptInviteView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.conf import settings

ns_invitations = patterns('',
    url(r'^accept-invite/(?P<key>\w+)/?$',AcceptInviteView.as_view(),name='accept-invite')
)

urlpatterns = patterns('',
    # Examples:
    url(r'^$', home, name='home'),
    url(r'^public_datasets/$', public_datasets, name='public_datasets'),
    url(r'^dashboard/$', login_required(dashboard), name='dashboard'),
    url(r'^geo/minority_businesses/',geo_minority_businesses,name='geo_minority_businesses'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^api/', include('api.urls'), name='api'),

    url(r'^request_an_invite/', InvitationRequestCreateView.as_view(), name='request_an_invite'),
    url(r'^request/(?P<pk>\d+)/(?P<slug>[-\w]+)/$',InvitationRequestDetailView.as_view(),name='invitation_request_detail'),

    url(r'^invite_user/', login_required(InviteUserView.as_view()), name='invite_user'),

    url(r'^dataset/create/', staff_member_required(DatasetCreateView.as_view()), name='dataset_create'),

    url(r'^captcha/', include('captcha.urls')),

    url(r'^api-token-auth/', views.obtain_auth_token),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^invitations/', include(ns_invitations, namespace="invitations")),
)

if settings.DEBUG:
        urlpatterns += patterns('',
            url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                'document_root': settings.MEDIA_ROOT,
            }),
            url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
                'document_root': settings.STATIC_ROOT,
            }),
    )